@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
    <div class="container p-3">
        <h5 class="mb-4">Employee Details</h5>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <a href="{{route('employees.create')}}" class="btn btn-primary d-inline-block mb-3 float-right">
                    <i class="fa fa-plus"> Create Employee</i>
                </a>
            </div>
        </div>
        <table id="employee_datatable" class="table table-striped table-bordered text-center" style="width:100%">
            <thead>
                <tr>
                    <th>Sr No.</th>
                    <th>Name</th>
                    <th>Operations</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($employees as $employee)
                    <tr>
                        <td></td>
                        <td>{{ $employee->name }}</td>
                        <td>
                            <a href="{{route('employees.show', $employee->id)}}" class="btn btn-outline-secondary mr-2"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="{{route('employees.edit', $employee->id)}}" class="btn btn-outline-primary mr-2"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <form class="d-inline" action="{{ route('employees.destroy', $employee->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" 
                                onclick="return confirm('Are you sure you want to delete ?');" 
                                class="btn btn-outline-danger">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>Sr No.</th>
                    <th>Name</th>
                    <th>Operations</th>
                </tr>
            </tfoot>
        </table>
    </div>

@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('js/index.js') }}"></script>
@endsection