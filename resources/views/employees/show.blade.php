@extends('layouts.app')

@section('content')
    <div class="container py-5">
        <h3 class="mb-4">View Employee</h3>
        <div id="emp_details_container">
            <h5>Employee Details</h5>
            <form id="emp_details_form" action="{{ route('employees.update', $employee->id) }}" method="POST">
                @csrf
                @method('PUT')

                {{-- START: EMPLOYEE DETAILS --}}
                <div class="row mb-5">
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="name" class="d-flex">Employee Name</label>
                            <input disabled id="name" class="form-control" type="text" placeholder="Enter your name" value="{{$employee->name}}" name="name">
                        </div>
                    </div>
                </div>
                {{-- END: EMPLOYEE DETAILS --}}

                {{-- START: ADDRESS DETAILS --}}
                <h6>Address Details</h6>
                <div class="row mb-3">
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="address_line_1" class="d-flex">Address 1 *</label>
                            <input id="address_line_1" 
                            class="form-control"
                            type="text" 
                            placeholder="Address 1" 
                            name="address_line_1"
                            value="{{$employee->address->address_line_1}}"
                            required
                            disabled>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="address_line_2" class="d-flex">Address 2</label>
                            <input id="address_line_2" 
                            class="form-control"
                            type="text" 
                            name="address_line_2" 
                            value="{{$employee->address->address_line_2}}"
                            placeholder="Address 2"
                            disabled>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="location" class="d-flex">Location</label>
                            <input id="location" 
                            class="form-control"
                            type="text" 
                            name="location" 
                            value="{{$employee->address->location}}"
                            placeholder="Location"
                            disabled>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="zip_code"  class="d-flex">Zip / Postal Code</label>
                            <input id="zip_code" 
                            class="form-control"
                            name="zip_code" 
                            value="{{$employee->address->zip_code}}"
                            type="text" 
                            placeholder="Zip / Postal Code"
                            disabled>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="emp_address_postal" class="d-flex">Postal Area</label>
                            <input id="emp_address_postal" 
                            class="form-control"
                            name="emp_address_postal" 
                            value="{{$employee->address->postal_area}}"
                            type="text" 
                            placeholder="Postal Area"
                            disabled>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="taluka" class="d-flex">Taluka</label>
                            <input id="taluka" 
                            class="form-control"
                            name="taluka" 
                            value="{{$employee->address->taluka}}"
                            type="text" 
                            placeholder="Taluka"
                            disabled>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="suburb" class="d-flex">Suburb</label>
                            <input id="suburb" 
                            class="form-control"
                            name="suburb" 
                            value="{{$employee->address->suburb}}"
                            type="text" 
                            placeholder="Suburb"
                            disabled>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="east_west" class="d-flex">East / West</label>
                            <input id="east_west" 
                            class="form-control"
                            name="east_west" 
                            value="{{$employee->address->east_west}}"
                            type="text" 
                            placeholder="East / West"
                            disabled>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="city" class="d-flex">City *</label>
                            <input id="city" 
                            class="form-control"
                            name="city" 
                            value="{{$employee->address->city}}"
                            type="text" 
                            placeholder="City" 
                            required
                            disabled>
                        </div>
                    </div>
                </div>

                <div class="row mb-5">
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="district" class="d-flex">District</label>
                            <input id="district"
                            class="form-control" 
                            name="district" 
                            value="{{$employee->address->district}}"
                            type="text" 
                            placeholder="District"
                            disabled>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="state" class="d-flex">State</label>
                            <input id="state" 
                            class="form-control"
                            name="state" 
                            value="{{$employee->address->state}}"
                            type="text" 
                            placeholder="State"
                            disabled>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <div class="form-group m-0">
                            <label for="country" class="d-flex">Country</label>
                            <input id="country" 
                            class="form-control"
                            name="country" 
                            value="{{$employee->address->country}}"
                            type="text" 
                            placeholder="Country" 
                            required
                            disabled>
                        </div>
                    </div>
                </div>
                {{-- END: ADDRESS DETAILS --}}

                {{-- START: CONTACT DETAILS --}}
                <h6>Contact Details</h6>
                <div class="row mb-3">
                    <div class="col-md-4">
                        

                        <div class="row">
                            <div class="col-md-12">
                                <label for="" class="m-0">Mobile Number</label>
                            </div>
                        </div>
                        <ul id="mobile_number_list" class="list-group list-unstyled" >
                        
                            @for ($i=0; $i<count($employee->mobileNumbers); $i++)
                            
                                <li id="" class="list-item">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input id="mobile_number_{{$i+1}}" 
                                            name="mobile_numbers[]" 
                                            value="{{$employee->mobileNumbers[$i]->mobile_number}}"
                                            class="mt-2 mb-4 mr-2 form-control w-50 d-inline-block" 
                                            type="text" 
                                            placeholder="0000000000"
                                            disabled>

                                            <input type="radio" 
                                            class="primary_mobile_number" 
                                            id="primary_mobile_number_{{$i+1}}" 
                                            name="primary_mobile_number_status"
                                            {{($employee->getPrimaryMobileNumber() == $employee->mobileNumbers[$i]->mobile_number) ? 'checked' : ''}}
                                            disabled>
                                            <label for="" class="m-0 mr-2">Primary</label>

                                            
                                        </div>
                                    </div>
                                </li>

                            @endfor

                        </ul>
                    </div>
                    <div class="col-md-4">


                        <div class="row">
                            <div class="col-md-12">
                                <label for="" class="m-0">Whatsapp Number</label>
                            </div>
                        </div>
                        <ul id="whatsapp_number_list" class="list-group list-unstyled" >

                            @for ($i=0; $i<count($employee->whatsappNumbers); $i++)

                            <li id="" class="list-item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="whatsapp_number_{{$i+1}}" 
                                        name="whatsapp_numbers[]" 
                                        value="{{$employee->whatsappNumbers[$i]->whatsapp_number}}"
                                        class="mt-2 mb-4 mr-2 form-control w-50 d-inline-block" 
                                        type="text" 
                                        placeholder="0000000000"
                                        disabled>
                                        
                                        <input type="radio" 
                                        id="primary_whatsapp_number_{{$i+1}}" 
                                        name="primary_whatsapp_number_status"
                                        {{($employee->getPrimaryWhatsappNumber() == $employee->whatsappNumbers[$i]->whatsapp_number) ? 'checked' : ''}}
                                        disabled>
                                        <label for="" class="m-0 mr-2">Primary</label>

                                        

                                    </div>
                                </div>
                            </li>

                            @endfor

                        </ul>
                    </div>
                    <div class="col-md-4">
                        
                        
                        <div class="row">
                            <div class="col-md-12">
                                <label for="" class="m-0">Email *</label>
                            </div>
                        </div>
                        <ul id="email_list" class="list-group list-unstyled" >

                            @for ($i=0; $i<count($employee->emails); $i++)

                            <li id="" class="list-item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="email_{{$i+1}}" 
                                        name="emails[]" 
                                        value="{{$employee->emails[$i]->email}}"
                                        class="mt-2 mb-4 mr-2 form-control w-50 d-inline-block" 
                                        type="text" 
                                        placeholder="123@example.com"
                                        disabled>

                                        <input type="radio" 
                                        id="primary_email_{{$i+1}}" 
                                        name="primary_email_status"
                                        {{($employee->getPrimaryEmail() == $employee->emails[$i]->email) ? 'checked' : ''}}
                                        disabled>
                                        <label for="" class="m-0">Primary</label>

                                        

                                    </div>
                                </div>
                            </li>

                            @endfor

                        </ul>
                    </div>
                </div>
                {{-- END: CONTACT DETAILS --}}


                <a href="{{route('employees.index')}}" class="btn btn-primary d-inline-block mr-3">Back</a>
                <a href="{{route('employees.edit', $employee->id)}}" class="btn btn-success d-inline-block mr-3">Edit</a>

            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="https://code.jquery.com/git/jquery-git.min.js"></script>
@endsection