<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = [
        'emp_id',
        'email',
    ];

    // RELATIONSHIP METHODS
    public function employee() {
        return $this->belongsTo(Employee::class, 'emp_id');
    }
}
