<?php

namespace App\Http\Controllers;

use App\Address;
use App\Email;
use App\Employee;
use App\MobileNumber;
use App\WhatsappNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        return view('employees.index', compact([
            'employees'
        ])
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->input());

        $this->insertEmployee($request);

        return redirect(route('employees.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('employees.show', compact([
            'employee'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('employees.edit', compact([
            'employee'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $employee->update([
            'name'  =>  $request->name
        ]);

        $address_data = $request->only([
            'address_line_1',
            'address_line_2',
            'location',
            'zip_code',
            'postal_area',
            'taluka',
            'suburb',
            'east_west',
            'city',
            'district',
            'state',
            'country',
        ]);

        $existing_address_attributes = $employee->address->getAttributes();
        $address_diff = array_diff_assoc($address_data, $existing_address_attributes);

        $employee->address()
                ->update($address_diff);

        
        /**
         * STEPS TO UPDATE MOBILE NUMBERS:
         * 
         * Check if the existing mobile number is present in the requested mobile number array
         * If yes, remove that number from the existing mobile number array and requested mobile number array
         * If not, replace the existing number with the requested mobile number and remove existing mobile number and requested mobile number from their arrays respectively 
         * Repeat this until the existing mobile number array and requested mobile number array is not empty
         * If existing mobile number array is empty, insert the remaining requested mobile numbers
         * If requested mobile number array is empty, delete the remaining existing mobile numbers
         */
        
        // Code to update mobile numbers
        $existing_mobile_numbers = $employee->getAllMobileNumbers();
        $requested_mobile_numbers = $request->mobile_numbers;

        // dd($existing_mobile_numbers, $requested_mobile_numbers);

        while($existing_mobile_numbers && $requested_mobile_numbers) {
            
            $indexOfRequest = array_search($existing_mobile_numbers[0], $requested_mobile_numbers);

            if($indexOfRequest !==  false) {
                
                if($existing_mobile_numbers[0] == $request->primary_mobile_number) {
                    $mobile_number_id = $employee->getMobileNumberIdFromNumber($existing_mobile_numbers[0]);
                    $this->updatePrimaryMobileNumberId($employee, $mobile_number_id);
                }

                array_splice($existing_mobile_numbers, 0, 1);
                array_splice($requested_mobile_numbers, $indexOfRequest, 1);

            } else {

                $mobile_number = $employee->getMobileNumberModelFromNumber($existing_mobile_numbers[0]);
                
                $mobile_number->update([
                    'mobile_number' =>  $requested_mobile_numbers[0]
                ]);

                if($requested_mobile_numbers[0] == $request->primary_mobile_number) {
                    // dd("UPDATING");
                    $this->updatePrimaryMobileNumberId($employee, $mobile_number->id);
                }

                array_splice($existing_mobile_numbers, 0, 1);
                array_splice($requested_mobile_numbers, 0, 1);
            }
        }
        
        if(!$existing_mobile_numbers) {
            // dd("Existing Number Array is empty");

            foreach($requested_mobile_numbers as $requested_mobile_number) {

                $mobile_number = MobileNumber::create([
                    'emp_id'        =>  $employee->id,
                    'mobile_number' =>  $requested_mobile_number
                ]);

                if($requested_mobile_number == $request->primary_mobile_number) {
                    $this->updatePrimaryMobileNumberId($employee, $mobile_number->id);
                }
            }
        }
    
        if(!$requested_mobile_numbers) {
            // dd("Requested Number Array is empty");

            foreach($existing_mobile_numbers as $existing_mobile_number) {

                $mobile_number = $employee->getMobileNumberModelFromNumber($existing_mobile_number);
                $mobile_number->delete();
            }
        }
        
        /*
         * ALTERNATIVE WAY
         * Delete the existing mobile numbers
         * Add mobile numbers
         */

        // $employee->mobileNumbers()->delete();

        // foreach($request->mobile_numbers as $mobile_number) {
        //     $mobileNumber = MobileNumber::create([
        //         'emp_id'        =>  $employee->id,
        //         'mobile_number' =>  $mobile_number,
        //     ]);
        //     if($mobileNumber->mobile_number == $request->primary_mobile_number) {
        //         $primary_mobile_number_id = $mobileNumber->id;
        //     }
        // }

        // Code to update whatsapp numbers
        $existing_whatsapp_numbers = $employee->getAllWhatsappNumbers();
        $requested_whatsapp_numbers = $request->whatsapp_numbers;

        while($existing_whatsapp_numbers && $requested_whatsapp_numbers) {
            
            $indexOfRequest = array_search($existing_whatsapp_numbers[0], $requested_whatsapp_numbers);

            if($indexOfRequest !== false) {

                if($existing_whatsapp_numbers[0] == $request->primary_whatsapp_number) {
                    $whatsapp_number_id = $employee->getWhatsappNumberIdFromNumber($existing_whatsapp_numbers[0]);
                    $this->updatePrimaryWhatsappNumberId($employee, $whatsapp_number_id);
                }

                array_splice($existing_whatsapp_numbers, 0, 1);
                array_splice($requested_whatsapp_numbers, $indexOfRequest, 1);

            } else {

                $whatsapp_number = $employee->getWhatsappNumberModelFromNumber($existing_whatsapp_numbers[0]);
                
                $whatsapp_number->update([
                    'whatsapp_number' =>  $requested_whatsapp_numbers[0]
                ]);

                if($requested_whatsapp_numbers[0] == $request->primary_whatsapp_number) {
                    $this->updatePrimaryWhatsappNumberId($employee, $whatsapp_number->id);
                }

                array_splice($existing_whatsapp_numbers, 0, 1);
                array_splice($requested_whatsapp_numbers, 0, 1);
            }
        }

        if(!$existing_whatsapp_numbers) {

            foreach($requested_whatsapp_numbers as $requested_whatsapp_number) {

                $whatsapp_number = WhatsappNumber::create([
                    'emp_id'            =>  $employee->id,
                    'whatsapp_number'   =>  $requested_whatsapp_number
                ]);

                if($requested_whatsapp_number == $request->primary_whatsapp_number) {
                    $this->updatePrimaryWhatsappNumberId($employee, $whatsapp_number->id);
                }
            }
        }

        if(!$requested_whatsapp_numbers) {
            foreach($existing_whatsapp_numbers as $existing_whatsapp_number) {
                $whatsapp_number = $employee->getWhatsappNumberModelFromNumber($existing_whatsapp_number);
                $whatsapp_number->delete();
            }
        }

        // Code to update emails
        $existing_emails = $employee->getAllEmails();
        $requested_emails = $request->emails;

        while($existing_emails && $requested_emails) {
            
            $indexOfRequest = array_search($existing_emails[0], $requested_emails);

            if($indexOfRequest !== false) {

                if($existing_emails[0] == $request->primary_email) {
                    $email_id = $employee->getEmailIdFromEmail($existing_emails[0]);
                    $this->updatePrimaryEmailId($employee, $email_id);
                }

                array_splice($existing_emails, 0, 1);
                array_splice($requested_emails, $indexOfRequest, 1);

            } else {

                $email = $employee->getEmailModelFromEmail($existing_emails[0]);
                
                $email->update([
                    'email' =>  $requested_emails[0]
                ]);

                if($requested_emails[0] == $request->primary_email) {
                    $this->updatePrimaryEmailId($employee, $email->id);
                }

                array_splice($existing_emails, 0, 1);
                array_splice($requested_emails, 0, 1);
            }
        }

        if(!$existing_emails) {

            foreach($requested_emails as $requested_email) {
            
                $email = Email::create([
                    'emp_id'    =>  $employee->id,
                    'email'     =>  $requested_email
                ]);

                if($requested_email == $request->primary_email) {
                    $this->updatePrimaryEmailId($employee, $email->id);
                }
            }
        }

        if(!$requested_emails) {
            foreach($existing_emails as $existing_email) {
                $email = $employee->getEmailModelFromEmail($existing_email);
                $email->delete();
            }
        }

        // $this->updateAttributesPrimaryId($employee, $request);

        // Normal flow
        // $this->deleteEmployee($employee);
        
        // $this->insertEmployee($request);

        return redirect(route('employees.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $this->deleteEmployee($employee);

        return redirect(route('employees.index'));
    }

    
    // HELPER FUNCTIONS
    private function updatePrimaryMobileNumberId(Employee $employee, Int $primary_mobile_number_id) {
        $employee->update([
            'primary_mobile_number_id'  =>  $primary_mobile_number_id
        ]);
    }

    private function updatePrimaryWhatsappNumberId(Employee $employee, Int $primary_whatsapp_number_id) {
        $employee->update([
            'primary_whatsapp_number_id'  =>  $primary_whatsapp_number_id
        ]);
    }

    private function updatePrimaryEmailId(Employee $employee, Int $primary_email_id) {
        $employee->update([
            'primary_email_id'  =>  $primary_email_id
        ]);
    }

    private function updateAttributesPrimaryId(Employee $employee, Request $request) {
        // Updating the primary_mobile_number_id in employees table
        $primary_mobile_number_id = $employee->getMobileNumberIdFromNumber($request->primary_mobile_number);
        $employee->update([
            'primary_mobile_number_id'  =>  $primary_mobile_number_id
        ]);

        // Updating the primary_whatsapp_number_id in employees table
        $primary_whatsapp_number_id = $employee->getWhatsappNumberIdFromNumber($request->primary_whatsapp_number);
        $employee->update([
            'primary_whatsapp_number_id'  =>  $primary_whatsapp_number_id
        ]);

        // Updating the primary_email_id in employees table
        $primary_email_id = $employee->getEmailIdFromEmail($request->primary_email);
        $employee->update([
            'primary_email_id'  =>  $primary_email_id
        ]);
    }

    private function insertEmployee(Object $request) {
        DB::beginTransaction();

        if($request->input()) {
            $employee = Employee::create([
                'name'  =>  $request->name,
            ]);

            if($employee->id) {
                $address = Address::create([
                    'emp_id'            =>  $employee->id,
                    'address_line_1'    =>  $request->address_line_1,
                    'address_line_2'    =>  $request->address_line_2,
                    'location'          =>  $request->location,
                    'zip_code'          =>  $request->zip_code,
                    'postal_area'       =>  $request->postal_area,
                    'taluka'            =>  $request->taluka,
                    'suburb'            =>  $request->suburb,
                    'east_west'         =>  $request->east_west,
                    'city'              =>  $request->city,
                    'district'          =>  $request->district,
                    'state'             =>  $request->state,
                    'country'           =>  $request->country,
                ]);

                if($address->id) {
                    foreach($request->mobile_numbers as $mobile_number) {
                        $mobileNumber = MobileNumber::create([
                            'emp_id'        =>  $employee->id,
                            'mobile_number' =>  $mobile_number,
                        ]);
                        if($mobileNumber->id == null) {
                            DB::rollBack();
                            return;
                        }
                        if($mobileNumber->mobile_number == $request->primary_mobile_number) {
                            $primary_mobile_number_id = $mobileNumber->id;
                        }
                    }

                    foreach($request->whatsapp_numbers as $whatsapp_number) {
                        $whatsappNumber = WhatsappNumber::create([
                            'emp_id'            =>  $employee->id,
                            'whatsapp_number'   =>  $whatsapp_number,
                        ]);
                        if($whatsappNumber->id == null) {
                            DB::rollBack();
                            return;
                        }
                        if($whatsappNumber->whatsapp_number == $request->primary_whatsapp_number) {
                            $primary_whatsapp_number_id = $whatsappNumber->id;
                        }
                    }

                    foreach($request->emails as $requested_email) {
                        $email = Email::create([
                            'emp_id'    =>  $employee->id,
                            'email'     =>  $requested_email,
                        ]);
                        if($email->id == null) {
                            DB::rollBack();
                            return;
                        }
                        if($email->email == $request->primary_email) {
                            $primary_email_id = $email->id;
                        }
                    }

                    $returnValue = $employee->update([
                        'primary_mobile_number_id'      =>  $primary_mobile_number_id, 
                        'primary_whatsapp_number_id'    =>  $primary_whatsapp_number_id,
                        'primary_email_id'              =>  $primary_email_id,
                    ]);

                    if($returnValue) {
                        DB::commit();
                    } else {
                        DB::rollBack();
                        return;
                    }
                } else {
                    DB::rollBack();
                    return;
                }
            } else {
                DB::rollBack();
                return;
            }
        } else {
            DB::rollBack();
            return;
        }

        /*if($request->input()) {
            $employee = new Employee;
            $employee->name = $request['emp_name'];
            $employee->save();
            $employee_id = DB::getPdo()->lastInsertId();

            if($employee_id) {
                $address = new Address;
                $address->emp_id = $employee_id;
                $address->address_line_1 = $request['emp_address_1'];
                $address->address_line_2 = $request['emp_address_2'];
                $address->location = $request['emp_address_location'];
                $address->zip_code = $request['emp_address_zip'];
                $address->postal_area = $request['emp_address_postal'];
                $address->taluka = $request['emp_address_taluka'];
                $address->suburb = $request['emp_address_suburb'];
                $address->east_west = $request['emp_address_east_west'];
                $address->city = $request['emp_address_city'];
                $address->district = $request['emp_address_district'];
                $address->state = $request['emp_address_state'];
                $address->country = $request['emp_address_country'];
                $address->save();
                $address_id = DB::getPdo()->lastInsertId();

                if($address_id) {
                    $mobileNumber = new MobileNumber;
                    foreach($request['mobile_numbers'] as $mobile_number){
                        $mobileNumber->emp_id = $employee_id;
                        $mobileNumber->mobile_number = $mobile_number;
                        $mobileNumber->save();
                        $inserted_mobile_number_id = DB::getPdo()->lastInsertId();
                        if($inserted_mobile_number_id) {
                            $mobileNumber = new MobileNumber;
                        } else {
                            DB::rollBack();
                        }
                    }

                    $whatsappNumber = new WhatsappNumber;
                    foreach($request['whatsapp_numbers'] as $whatsapp_number){
                        $whatsappNumber->emp_id = $employee_id;
                        $whatsappNumber->whatsapp_number = $whatsapp_number;
                        $whatsappNumber->save();
                        $inserted_whatsapp_number_id = DB::getPdo()->lastInsertId();
                        if($inserted_whatsapp_number_id) {
                            $whatsappNumber = new WhatsappNumber;
                        } else {
                            DB::rollBack();
                        }
                    }

                    $email = new Email;
                    foreach ($request['emails'] as $requested_email) {
                        $email->emp_id = $employee_id;
                        $email->email = $requested_email;
                        $email->save();
                        $inserted_email_id = DB::getPdo()->lastInsertId();
                        if($inserted_email_id){
                            $email = new Email; 
                        } else {
                            DB::rollBack();
                        }
                    }

                    $mobileNumber = $request['primary_mobile_number'];
                    $whatsappNumber = $request['primary_whatsapp_number'];
                    $email = $request['primary_email'];

                    $primary_mobile_number_id = DB::select("SELECT id FROM mobile_numbers WHERE emp_id = $employee_id AND mobile_number = $mobileNumber")[0]->id;

                    $primary_whatsapp_number_id = DB::select("SELECT id FROM whatsapp_numbers WHERE emp_id = $employee_id AND whatsapp_number = $whatsappNumber")[0]->id;

                    $primary_email_id = DB::select("SELECT id FROM emails WHERE emp_id = $employee_id AND email = '$email'")[0]->id;
                    
                    if($primary_mobile_number_id && $primary_whatsapp_number_id && $primary_email_id) {
                        $returnValue = DB::table('employees')
                            ->where('id', '=', $employee_id )
                            ->update([
                                'primary_mobile_number_id' => $primary_mobile_number_id, 
                                'primary_whatsapp_number_id' => $primary_whatsapp_number_id,
                                'primary_email_id' => $primary_email_id
                            ]);

                        if($returnValue) {
                            DB::commit();
                        } else {
                            DB::rollBack();
                        }
                        
                    } else {
                        DB::rollBack();   
                    }
                } else {
                    DB::rollBack();    
                }
            } else {
                DB::rollBack();
            }
        } else {
            DB::rollBack();
        }*/
    }

    private function deleteEmployee(Employee $employee) {
        $employee->delete();
    }
}

/********************************************************/

/*
WORKING OF array_diff_assoc()

$array_1 = [
    'taluka'        =>  'Thane',
    'suburb'        =>  'Thane',
    'postal_area'   =>  'Thane',
    'state'         =>  'Maharashtra'
];

$array_2 = [
    'taluka'        =>  'Mumbai',
    'suburb'        =>  'Mumbai',
    'postal_area'   =>  'Mumbai',
    'country'       =>  'Maharashtra'
];

dd(array_diff_assoc($array_1, $array_2));
*/

/*
WORKING OF array_splice()

$array_1 = [
    '1',
    '2',
    '3',
    '4'
];

var_dump($array_1);

array_splice($array_1, 1, 1);

dd($array_1);

******IMPORTANT NOTE******
    - array_splice() returns false if the element is not found in the array and returns index if element is found
    - For instance: if the element is found at index zero(0) and you check the index in some condition like if($index); it will consider it as false and the condition won't be satisfied even if the element exists in the array as value for false is zero(0) or vice versa
    - So, to tackle this situation use strict checking i.e. '$index !== false'
    - Following is the example for it

// $array_1 = [1, 2, 3];

// $index = array_search(1, $array_1);

AMBIGUITY 
// if($index >= 0) {
//     dd("Here: ", $index);
// } else{
//     dd("Failed");
// }

AMBIGUITY 
// if($index != false) {
//     dd("Here: ", $index);
// } else{
//     dd("Failed");
// }

SOLUTION
// if($index !== false) {
//     dd("Here: ", $index);
// } else{
//     dd("Failed");
// }
*/