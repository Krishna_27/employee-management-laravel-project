<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileNumber extends Model
{
    protected $fillable = [
        'emp_id',
        'mobile_number'
    ];

    // RELATIONSHIP METHODS
    public function employee() {
        return $this->belongsTo(Employee::class, 'emp_id');
    }
}
