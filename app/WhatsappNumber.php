<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatsappNumber extends Model
{
    protected $fillable = [
        'emp_id',
        'whatsapp_number',
    ];

    // RELATIONSHIP METHODS
    public function employee() {
        return $this->belongsTo(Employee::class, 'emp_id');
    }
}
