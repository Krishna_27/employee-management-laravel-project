<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'name',
        'primary_mobile_number_id',
        'primary_whatsapp_number_id',
        'primary_email_id',
    ];

    // RELATIONSHIP METHODS
    public function address() {
        return $this->hasOne(Address::class, 'emp_id');
    }

    public function mobileNumbers() {
        return $this->hasMany(MobileNumber::class, 'emp_id');
    }

    public function whatsappNumbers() {
        return $this->hasMany(WhatsappNumber::class , 'emp_id');
    }

    public function emails() {
        return $this->hasMany(Email::class, 'emp_id');
    }

    // HELPER METHODS
    public function getPrimaryMobileNumber() {
        return $this->mobileNumbers
                    ->find($this->primary_mobile_number_id)
                    ->mobile_number;
    }

    public function getPrimaryWhatsappNumber() {
        return $this->whatsappNumbers
                    ->find($this->primary_whatsapp_number_id)
                    ->whatsapp_number;
    }

    public function getPrimaryEmail() {
        return $this->emails
                    ->find($this->primary_email_id)
                    ->email;
    }

    public function getAllPrimaryAttributes() {
        return $primaryAttributes = [
            'primary_mobile_number'     =>  $this->getPrimaryMobileNumber(),
            'primary_whatsapp_number'   =>  $this->getPrimaryWhatsappNumber(),
            'primary_email'             =>  $this->getPrimaryEmail(),
        ];
    }

    public function getAllMobileNumbers() {
        $mobile_numbers = $this->mobileNumbers
                                ->all();
                                
        $mobileNumbers = array();

        foreach($mobile_numbers as $mobile_number) {
            array_push($mobileNumbers, $mobile_number->mobile_number);
        }

        return $mobileNumbers;
    }

    public function getAllWhatsappNumbers() {
        $whatsapp_numbers = $this->whatsappNumbers
                                ->all();

        $whatsappNumbers = array();

        foreach($whatsapp_numbers as $whatsapp_number) {
            array_push($whatsappNumbers, $whatsapp_number->whatsapp_number);
        }

        return $whatsappNumbers;
    }

    public function getAllEmails() {
        $emails = $this->emails
                        ->all();

        $emailsArray = array();

        foreach($emails as $email) {
            array_push($emailsArray, $email->email);
        }

        return $emailsArray;
    }

    public function getMobileNumberModelFromNumber($mobile_number) {
        return $this->mobileNumbers()
            ->where(
                'mobile_number', 
                $mobile_number)
            ->first();
    }

    public function getWhatsappNumberModelFromNumber($whatsapp_number) {
        return $this->whatsappNumbers()
                    ->where('whatsapp_number', $whatsapp_number)
                    ->first();
    }

    public function getEmailModelFromEmail($email) {
        return $this->emails()
                    ->where('email', $email)
                    ->first();
    }

    public function getMobileNumberIdFromNumber($mobile_number) {
        return $this->mobileNumbers
                    ->where('mobile_number', $mobile_number)
                    ->first()
                    ->id;
    }

    public function getWhatsappNumberIdFromNumber($whatsapp_number) {
        return $this->whatsappNumbers
                    ->where('whatsapp_number', $whatsapp_number)
                    ->first()
                    ->id;
    }

    public function getEmailIdFromEmail($email) {
        return $this->emails
                    ->where('email', $email)
                    ->first()
                    ->id;
    }
}
