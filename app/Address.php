<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'emp_id',
        'address_line_1',
        'address_line_2',
        'location',
        'zip_code',
        'postal_area',
        'taluka',
        'suburb',
        'east_west',
        'city',
        'district',
        'state',
        'country',
    ];

    // RELATIONSHIP METHODS
    public function employee() {
        return $this->belongsTo(Employee::class, 'emp_id');
    }
}
