$(document).ready(function() {
    $('#employee_datatable').DataTable({
        "columns": [
            { "width": "20%" },
            { "width": "40%" },
            { "width": "40%" },
          ],
          'columnDefs': [ {
            'targets': [2], // column index (start from 0)
            'orderable': false, // set orderable false for selected columns
         }],
        "fnRowCallback" : function(nRow, aData, iDisplayIndex){
            $("td:first", nRow).html(iDisplayIndex +1);
            return nRow;
       }
    });
} );