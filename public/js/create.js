$('#btn_add_mobile_number_field')[0].setAttribute('onclick', 'addMobileNumber()')

mobileNumberCount = $('#mobile_number_list')[0].children.length + 1

function addMobileNumber() {
    $('#mobile_number_list').append(`<li id="" class="list-item">
    <div class="row">
        <div class="col-md-12">
            <input id=mobile_number_` + mobileNumberCount + `" 
            class="mt-2 mb-4 mr-2 form-control w-50 d-inline-block" 
            name="mobile_numbers[]"
            type="text" 
            placeholder="0000000000">
            <input class="primary_mobile_number"
            id="primary_mobile_number_` + mobileNumberCount + `" 
            type="radio" 
            name="primary_mobile_number_status">
            <label for="" class="mb-0 mr-2">Primary</label>
            <div type="button" 
            class="btn btn-outline-danger btn-small btn_remove_mobile_number_field" 
            onClick="removeMobileNumber(this)">
                <i>-</i>
            </div>
        </div>
    </div>
</li>`)
    mobileNumberCount++
}

function removeMobileNumber(btnRemoveMobileNumber) {
    if((($('#mobile_number_list')[0].children.length) == 1) || 
    (btnRemoveMobileNumber.previousElementSibling.previousElementSibling.checked)) {
        $('#primary_mobile_number_1')[0].checked = true
    }
    btnRemoveMobileNumber.parentElement.parentElement.parentElement.remove()
    // mobileNumberCount--
}


$('#btn_add_whatsapp_number_field')[0].setAttribute('onclick', 'addWhatsappNumber()')

whatsappNumberCount = $('#whatsapp_number_list')[0].children.length + 1

function addWhatsappNumber() {
    $('#whatsapp_number_list').append(`<li id="" class="list-item">
    <div class="row">
        <div class="col-md-12">
            <input id="whatsapp_number_` + whatsappNumberCount + `" 
            class="mt-2 mb-4 mr-2 form-control w-50 d-inline-block" 
            name="whatsapp_numbers[]" 
            type="text" 
            placeholder="0000000000">
            <input id="primary_whatsapp_number_` + whatsappNumberCount + `" 
            type="radio" 
            name="primary_whatsapp_number_status">
            <label for="" class="mb-0 mr-2">Primary</label>
            <div type="button" 
            class="btn btn-outline-danger btn-small btn_remove_whatsapp_number_field" 
            onClick="removeWhatsappNumber(this)">
                <i>-</i>
            </div>
        </div>
    </div>
</li>`)
    whatsappNumberCount++
}

function removeWhatsappNumber(btnRemoveWhatsappNumber) {
    if((($('#whatsapp_number_list')[0].children.length) == 1) || 
    (btnRemoveWhatsappNumber.previousElementSibling.previousElementSibling.checked)) {
        $('#primary_whatsapp_number_1')[0].checked = true
    }
    btnRemoveWhatsappNumber.parentElement.parentElement.parentElement.remove()
    // whatsappNumberCount--
}


$('#btn_add_email_field')[0].setAttribute('onclick', 'addEmail()')

emailCount = $('#email_list')[0].children.length + 1

function addEmail() {
    $('#email_list').append(`<li id="" class="list-item">
        <div class="row">
            <div class="col-md-12">
                <input id="email_` + emailCount + `" 
                class="mt-2 mb-4 mr-2 form-control w-50 d-inline-block" 
                name="emails[]" 
                type="text" 
                placeholder="123@example.com">
                <input id="primary_email_` + emailCount + `" 
                type="radio" 
                name="primary_email_status">
                <label for="" class="mb-0 mr-2">Primary</label>
                <div type="button" 
                class="btn btn-outline-danger btn-small btn_remove_email_field" 
                onClick="removeEmail(this)">
                    <i>-</i>
                </div>
            </div>
        </div>
    </li>`
    )
    emailCount++
}

function removeEmail(btnRemoveEmail) {
    if((($('#email_list')[0].children.length) == 1) || 
    (btnRemoveEmail.previousElementSibling.previousElementSibling.checked)) {
        $('#primary_email_1')[0].checked = true
    }
    btnRemoveEmail.parentElement.parentElement.parentElement.remove()
    // emailCount--
}

$('#emp_details_form').on('submit', function(e){
    e.preventDefault();
    mobileNumberList = $('#mobile_number_list')[0]
    mobileNumberListItems = mobileNumberList.children
    
    for(mobileNumberListItem of mobileNumberListItems) {
        radioButton = mobileNumberListItem.children[0].children[0].children[1]
        if(radioButton.checked) {
            inputField = mobileNumberListItem.children[0].children[0].children[0]
            $('#primary_mobile_number')[0].value = inputField.value
        }
    }

    whatsappNumberList = $('#whatsapp_number_list')[0]
    whatsappNumberListItems = whatsappNumberList.children
    
    for(whatsappNumberListItem of whatsappNumberListItems) {
        radioButton = whatsappNumberListItem.children[0].children[0].children[1]
        if(radioButton.checked) {
            inputField = whatsappNumberListItem.children[0].children[0].children[0]
            $('#primary_whatsapp_number')[0].value = inputField.value
        }
    }

    emailList = $('#email_list')[0]
    emailListItems = emailList.children
    
    for(emailListItem of emailListItems) {
        radioButton = emailListItem.children[0].children[0].children[1]
        if(radioButton.checked) {
            inputField = emailListItem.children[0].children[0].children[0]
            $('#primary_email')[0].value = inputField.value
        }
    }

    this.submit()
});