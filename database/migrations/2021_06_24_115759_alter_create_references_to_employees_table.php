<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCreateReferencesToEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->unsignedBigInteger('primary_mobile_number_id')->nullable();
            $table->unsignedBigInteger('primary_whatsapp_number_id')->nullable();
            $table->unsignedBigInteger('primary_email_id')->nullable();

            $table->foreign('primary_mobile_number_id')
                ->references('id')
                ->on('mobile_numbers')
                ->onDelete('set null');

            $table->foreign('primary_whatsapp_number_id')
                ->references('id')
                ->on('whatsapp_numbers')
                ->onDelete('set null');

            $table->foreign('primary_email_id')
                ->references('id')
                ->on('emails')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            //
        });
    }
}
